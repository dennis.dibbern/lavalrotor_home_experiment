"""Functions for processing measurement data"""

import numpy as np
from numpy.fft import fft, ifft
from typing import Tuple


def get_vec_accel(x: np.ndarray, y: np.ndarray, z: np.ndarray) -> np.ndarray:
    """Calculates the vector absolute value of the temporal evolution of a vector (x, y, z).

    Args:
        x (ndarray): Vector containing the temporal elements in the first axis direction.
        y (ndarray): Vector containing the temporal elements in the second axis direction.
        z (ndarray): Vector containing the temporal elements in the third axis direction.

    Returns:
        (ndarray): Absolute value of the evolution.
    """
    x_acc_sq = np.square(x)
    y_acc_sq = np.square(y)
    z_acc_sq = np.square(z)

    abs_acc = np.sqrt(x_acc_sq + y_acc_sq + z_acc_sq)

    return abs_acc

def interpolation(time: np.ndarray, data: np.ndarray) -> Tuple[np.ndarray, np.ndarray]:
    """Linearly interpolates values in data.

    Uses linear Newtonian interpolation. The interpolation points are distributed linearly over the
    entire time (min(time) to max(time)).

    Args:
        time (ndarray): Timestamp of the values in data.
        data (ndarray): Values to interpolate.

    Returns:
        (ndarray): Interpolation points based on 'time'.
        (ndarray): Interpolated values based on 'data'.
    """
    interpolation_time = np.linspace(np.min(time), np.max(time), len(time))
    interpolated_data = np.interp(interpolation_time, time, data)
    return interpolated_data

def my_fft(x: np.ndarray, time: np.ndarray) -> Tuple[np.ndarray, np.ndarray]:
    """Calculates the FFT of x using the numpy function fft()

    Args:
        x (ndarray): Measurement data that is transformed into the frequency range.
        time (ndarray): Timestamp of the measurement data.

    Returns:
        (ndarray): Amplitude of the computed FFT spectrum.
        (ndarray): Frequency of the computed FFT spectrum.
    """
    mittelwert = np.mean(x)
    daten_null_mittelwert = x-mittelwert


    x = daten_null_mittelwert    # altes x überschreiben
    sr = len(time)/(np.max(time)-np.min(time))

    X = fft(x)
    N = len(X)
    n = np.arange(N)
    T = N/sr
    freq = n/T 

# Get the one-sided specturm
    n_oneside = N//2
# get the one side frequency
    f_oneside = freq[:n_oneside]

# normalize the amplitude
    X_oneside =X[:n_oneside]/n_oneside
    
    return X_oneside, f_oneside